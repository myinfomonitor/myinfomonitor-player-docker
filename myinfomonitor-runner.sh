#!/bin/bash

xset s off -dpms

ulimit -i 65535

/usr/bin/soundserver-runner.sh &

sleep 10

connectedOutputs=$(xrandr | grep " connected" | sed -e "s/\([A-Z0-9]\+\) connected.*/\1/")
for display in $connectedOutputs
do
	xrandr --output $display --auto
done

if [ -n "$MIM_XRANDR_ARGS" ]; then
	echo "Applying custom display attributes using xrandr:" ${MIM_XRANDR_ARGS}
  	xrandr ${MIM_XRANDR_ARGS}
fi

if [ -n "$MIM_AUDIO_CMD" ]; then
	echo "Executing custom audio setup command(s):" ${MIM_AUDIO_CMD}
	${MIM_AUDIO_CMD}
fi

if [ -n "$MIM_CUSTOM_STARTUP_CMD" ]; then
	echo "Executing custom startup command(s): " ${MIM_CUSTOM_STARTUP_CMD}
    ${MIM_CUSTOM_STARTUP_CMD}
fi

if [ -n "$MIM_TIMEZONE" ]; then
  if [ -f "/usr/share/zoneinfo/$MIM_TIMEZONE" ]; then
    echo "Changing timezone to : " ${MIM_TIMEZONE}
    ln -sf /usr/share/zoneinfo/${MIM_TIMEZONE} /etc/localtime
	else
	  echo "Timezone ${MIM_TIMEZONE} not supported! Check /usr/share/zoneinfo for supported timezones."
  fi
fi

if [ -n "$MIM_LOCALE" ]; then
	echo "Setting locale (with localectl) to:" ${MIM_LOCALE}
	localectl ${MIM_LOCALE}
fi


QTWEBENGINE_CHROMIUM_FLAGS="--no-sandbox" /usr/bin/myinfomonitor -u ${RESIN_DEVICE_UUID:0:7} ${MIM_PLAYER_ARGS} "$@"
