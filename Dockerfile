FROM balenalib/intel-nuc-fedora:30 AS dist
SHELL ["/bin/bash", "-c"]

ENV TZ "Europe/Helsinki"

RUN dnf install -y https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm && dnf update -y
RUN dnf install -y  \
    wget \
    # QT WebEngine
    qt5-qtwebengine-freeworld \
    # This pulls in QtQuick, libgstreamer1.0 etc https://packages.debian.org/buster/libqt5multimedia5-plugins
    qt5-qtquickcontrols \
    qt5-qtquickcontrols2 \
    qt5-qtmultimedia \
    # This pulls in Xorg server https://packages.debian.org/buster/xserver-xorg-input-libinput
    xorg-x11-drv-libinput \
    # Xorg support for synaptics touch screens
    xorg-x11-drv-synaptics-legacy \
    # D-Bus support for Xorg, pulls in D-Bus https://packages.debian.org/buster/dbus-x11
    dbus-x11 \
    findutils \
    # GStreamer 1.0
    gstreamer1-libav \
    gstreamer1-plugins-bad-freeworld \
    gstreamer1-plugins-good-qt \
    gstreamer1-plugins-ugly \
    gstreamer-plugins-bad-free \
    # QTAv module for video playback
    qtav-qml-module \
    # JACK Audio Connection Kit (default server package)
    pulseaudio-module-jack \
    # liblame is already pulled in by gstreamer plugins, but lets add these just-in-case
    lame \
    faad2 \
    # VAAPI driver for Intel G45 & HD Graphics family
    libva-intel-hybrid-driver \
    libva-intel-driver \
    gstreamer1-vaapi \
    xorg-x11-drv-intel \
    intel-media-driver \
    # some generic network cli tools
    iputils \
    # for debugging: Video Acceleration (VA) API for Linux -- info program
    libva-utils \
    # This package contains various utilities for inspecting and setting of devices connected to the PCI bus.
    # https://packages.debian.org/en/buster/pciutils
    pciutils \
    # Cairo 2D vector graphics library (GObject library)
    # Pulls in cairo
    cairo-gobject \
    # Mesa Off-screen rendering extension
    mesa-libOSMesa \
    mesa-libEGL \
    mesa-libGL \
    mesa-libGLES \
    mesa-libOpenCL \
    mesa-vulkan-drivers \
    # tools for debugging the Intel graphics driver
    intel-gpu-tools \
    # PulseAudio sound server https://packages.debian.org/buster/pulseaudio
    pulseaudio \
    pulseaudio-module-x11 \
    pulseaudio-utils \
    # Volume control for PulseAudio
    pavucontrol \
    # ALSA
    alsa-utils \
    # time zone and daylight-saving time data
    tzdata \
    # this might be a dep required by Qt 5.12.x and later WebEngines
    libxslt \
    # startx etc
    xorg-x11-xinit \
    # X server utilities
    # Xrandr, xset etc
    # https://launchpad.net/ubuntu/bionic/+package/x11-xserver-utils
    xorg-x11-utils \
    # lsof command
    lsof \
    # Brings in fuser for example
    psmisc \
    # Tool for reporting for example mobo version for debugging
    dmidecode
# enable https://github.com/silenc3r/fedora-better-fonts
RUN dnf install -y dnf-plugins-core && dnf copr enable -y dawid/better_fonts && dnf install -y fontconfig-enhanced-defaults fontconfig-font-replacements
RUN dnf clean all \
    && rm -rf /var/cache/yum

CMD tail -f /dev/null


FROM dist AS build

SHELL ["/bin/bash", "-c"]

# Build MIM Player
# Here we dont care about layer size as this layer is discarded from
# final build anyways, so focusing on caching here
RUN dnf builddep -y qt5-qtwebengine-freeworld
RUN dnf install -y \
  @development-tools \
  qt5-qtwebengine-devel \
  NetworkManager-libnm-devel \
   glib2-devel

ADD https://currentmillis.com/time/minutes-since-unix-epoch.php /tmp/bustcache

#ARG SSH_PRIVATE_KEY
#RUN mkdir /root/.ssh/ \
#    && chmod 600 /root/.ssh \
#    && echo "${SSH_PRIVATE_KEY}" > /root/.ssh/id_rsa \
#    && chmod 400 /root/.ssh/id_rsa \
#    && touch /root/.ssh/known_hosts \
#    && ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts \
#    && cd /tmp \
#    && GIT_SSH_COMMAND="ssh -i /root/.ssh/id_rsa" git clone git@bitbucket.org:cemronltd/infomonitor-player-qt.git player-src  \
#    && mkdir /tmp/player-bin/ \
#    && cd /tmp/player-bin \
#    && qmake-qt5 ../player-src/myinfomonitor.pro \
#    && make \
#    && rm -rf /root/.ssh

RUN mkdir /root/.ssh/ \
    && chmod 600 /root/.ssh \
    && cat /run/secrets/bitbucket_mim_player_builder > /root/.ssh/id_rsa \
    && chmod 400 /root/.ssh/id_rsa \
    && touch /root/.ssh/known_hosts \
    && ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts \
    && cd /tmp \
    && GIT_SSH_COMMAND="ssh -i /root/.ssh/id_rsa" git clone git@bitbucket.org:cemronltd/infomonitor-player-qt.git player-src \
    && rm -rf /root/.ssh

RUN mkdir /tmp/player-bin/ \
    && cd /tmp/player-bin \
    && qmake-qt5 "CONFIG+=debug" "CONFIG+=qml_debug" ../player-src/myinfomonitor.pro \
    && make

RUN ls /tmp/player-bin

# this is for debugging the build, not used in last phase
CMD tail -f /dev/null

FROM balenalib/intel-nuc-fedora:30

COPY --from=dist / /
COPY --from=build /tmp/player-bin/myinfomonitor /usr/bin/

RUN mkdir -p /etc/X11/xinit \
  && echo "#!/bin/bash" > /etc/X11/xinit/xserverrc \
  && echo "" >> /etc/X11/xinit/xserverrc \
  && echo 'exec /usr/bin/X -s 0 dpms -nocursor -nolisten tcp "$@"' >> /etc/X11/xinit/xserverrc

RUN sed -i /xterm/s/^/#/ /etc/X11/xinit/Xsession

COPY myinfomonitor-runner.sh /usr/bin/
COPY myinfomonitor-start.sh /usr/bin/
COPY soundserver-runner.sh /usr/bin/
# This is used if Pulseaudio is running in per-user mode
COPY default.pa /etc/pulse/
# This is used if Pulseaudio is running with --system
COPY system.pa /etc/pulse/
RUN chmod +x /usr/bin/myinfomonitor* && chmod +x /usr/bin/soundserver-runner.sh
RUN usermod -a -G audio pulse

#RUN echo "default-server = unix:/tmp/pulseserver" >> /etc/pulse/client.conf
RUN echo "default-server=/var/run/pulse/native" >> /etc/pulse/client.conf
RUN echo "autospawn = no" >> /etc/pulse/client.conf

# Make analog audio work when the system has something like an ALC662 for analog outputs
RUN echo "options snd-hda-intel model=laptop" >> /etc/modprobe.d/alsa.conf
#RUN echo "load-module module-native-protocol-unix auth-anonymous=1 socket=/tmp/pulseserver" >> /etc/pulse/default.pa

RUN usermod -a -G pulse,pulse-access,audio root && usermod -a -G audio pulse
#RUN chmod +x /usr/bin/resin-jack-runner.sh

ENV DISPLAY :0
ENV DBUS_SYSTEM_BUS_ADDRESS unix:path=/host/run/dbus/system_bus_socket
ENV UDEV=1

RUN ln -sf /usr/share/zoneinfo/Europe/Helsinki /etc/localtime

CMD ["bash", "/usr/bin/myinfomonitor-start.sh"]

