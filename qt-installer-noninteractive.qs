// Emacs mode hint: -*- mode: JavaScript -*-

function Controller() {
    installer.autoRejectMessageBoxes();
    installer.installationFinished.connect(function() {
 	console.log("installationFinished");
        gui.clickButton(buttons.NextButton);
    })
}

Controller.prototype.WelcomePageCallback = function() {
    console.log("WelcomePageCallback");
    gui.clickButton(buttons.NextButton, 3000);
}

Controller.prototype.CredentialsPageCallback = function() {
    console.log("CredentialsPageCallback");
    gui.clickButton(buttons.NextButton);
}

Controller.prototype.IntroductionPageCallback = function() {
    console.log("IntroductionPageCallback");
    gui.clickButton(buttons.NextButton);
}

Controller.prototype.TargetDirectoryPageCallback = function()
{
    console.log("TargetDirectoryPageCallback");
    gui.currentPageWidget().TargetDirectoryLineEdit.setText("/opt/Qt5");
    gui.clickButton(buttons.NextButton,100);
}

Controller.prototype.ComponentSelectionPageCallback = function() {
    console.log("ComponentSelectionPageCallback");
    var widget = gui.currentPageWidget();
    var qtVersion="qt.qt5.5125";
    widget.deselectAll();
    widget.selectComponent("qt");
    widget.selectComponent(qtVersion);
    widget.selectComponent(qtVersion+".qtwebengine");
    widget.deselectComponent(qtVersion+".qtcharts");
    widget.deselectComponent(qtVersion+".qtdatavis3d");
    widget.deselectComponent(qtVersion+".qtpurchasing");
    widget.deselectComponent(qtVersion+".qtnetworkauth");
    widget.deselectComponent(qtVersion+".qtremoteobjects");
    widget.deselectComponent(qtVersion+".qtremoteobjects");
    widget.deselectComponent(qtVersion+".qtscript");
    widget.deselectComponent(qtVersion+".android_x86");
    widget.deselectComponent(qtVersion+".android_armv7");
    widget.deselectComponent(qtVersion+".doc");
    widget.deselectComponent(qtVersion+".src");
    widget.deselectComponent(qtVersion+".examples");
    widget.deselectComponent(qtVersion+".tools");


    // widget.deselectComponent("qt.tools.qtcreator");
    // widget.deselectComponent("qt.55.qt3d");
    // widget.deselectComponent("qt.55.qtcanvas3d");
    // widget.deselectComponent("qt.55.qtlocation");
    // widget.deselectComponent("qt.55.qtquick1");
    // widget.deselectComponent("qt.55.qtscript");
    // widget.deselectComponent("qt.55.qtwebengine");
    // widget.deselectComponent("qt.extras");
    // widget.deselectComponent("qt.tools.doc");
    // widget.deselectComponent("qt.tools.examples");

    gui.clickButton(buttons.NextButton);
}

Controller.prototype.LicenseAgreementPageCallback = function() {
    console.log("LicenseAgreementPageCallback");
    gui.currentPageWidget().AcceptLicenseRadioButton.setChecked(true);
    gui.clickButton(buttons.NextButton);
}

Controller.prototype.StartMenuDirectoryPageCallback = function() {
    console.log("StartMenuDirectoryPageCallback");
    gui.clickButton(buttons.NextButton);
}

Controller.prototype.ReadyForInstallationPageCallback = function()
{
    console.log("ReadyForInstallationPageCallback");
    gui.clickButton(buttons.NextButton);
}

Controller.prototype.FinishedPageCallback = function() {
    console.log("FinishedPageCallback");
var checkBoxForm = gui.currentPageWidget().LaunchQtCreatorCheckBoxForm
if (checkBoxForm && checkBoxForm.launchQtCreatorCheckBox) {
    checkBoxForm.launchQtCreatorCheckBox.checked = false;
}
    gui.clickButton(buttons.FinishButton);
}

