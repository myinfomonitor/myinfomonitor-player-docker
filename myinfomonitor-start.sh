#!/bin/bash

# By default docker gives us 64MB of shared memory size but to display heavy
# pages we need more.
umount /dev/shm && mount -t tmpfs shm /dev/shm

rm /tmp/.X0-lock &>/dev/null || true

CUSR=root

CONFIGDIR="/data/${CUSR}/config"
CACHEDIR="/data/${CUSR}/cache"

if [[ ! -e $CONFIGDIR ]]; then
	mkdir -p $CONFIGDIR
fi

if [[ ! -e $CACHEDIR ]]; then
	mkdir -p $CACHEDIR
fi

rm -rf root/.config &>/dev/null || true
ln -s $CONFIGDIR /root/.config &>/dev/null  || true

if [ "$MIM_PURGE_CACHE_ON_BOOT" != "false" ]; then
  echo "Purging cache..."
  if [[ -e /root/.cache ]]; then
    rm -rf root/.cache &>/dev/null || true
  fi
  rm -rf ${CACHEDIR:?}/* &>/dev/null || true
fi

ln -s $CACHEDIR /root/.cache &> /dev/null || true

export XDG_RUNTIME_DIR=$CACHEDIR

/usr/bin/startx /usr/bin/myinfomonitor-runner.sh "$@"
