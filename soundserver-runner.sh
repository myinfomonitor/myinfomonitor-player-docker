#!/bin/bash
# first lets add the pulse user to the host audio group (which we create first, because host has
# a different id for the group than we have here in the container)
AUDIOGROUP=$(stat -c '%g' /dev/snd/controlC0)
groupadd -g "$AUDIOGROUP" hostaudio && usermod -a -G hostaudio pulse

mkdir -p /var/run/pulse
chown -R pulse:pulse /var/run/pulse

export MIM_PULSEAUDIO_ARGS="--log-level=1 --log-target=stderr --disallow-module-loading --disallow-exit=true --exit-idle-time=180 --realtime  --system"
if [ -n "$MIM_CUSTOM_PULSEAUDIO_ARGS" ]; then
    export MIM_PULSEAUDIO_ARGS=${MIM_CUSTOM_PULSEAUDIO_ARGS}
fi

echo "Pulseaudio args ${MIM_PULSEAUDIO_ARGS}"
#then lets run pulse
for ((;;))
do
  DBUS_SYSTEM_BUS_ADDRESS=unix:path=/host/run/dbus/system_bus_socket /usr/bin/pulseaudio ${MIM_PULSEAUDIO_ARGS}
  echo "Sound server died, respawning..."
  sleep 1
done
