#!/bin/bash
cd ../myinfomonitor-player-qt
git pull
cd ../myinfomonitor-player-docker
qmake ../myinfomonitor-player-qt/myinfomonitor.pro
make
rsync -r --exclude "*.debug" ../qt5/qt-everywhere-src-5.11.2/lib .
sudo docker build "$@"
